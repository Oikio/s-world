'use strict';
const world = require('./world');
module.exports = function initWorld() {
  var edd = Object.create(world.entities.human).init({name: 'edd', gender: 'male'});
  var tedd = Object.create(world.entities.humanWithSecret).init({name: 'tedd', gender: 'male'});
  var home = Object.create(world.entities.house).init({pos: [3, 0, 3]});

  world.addEntity(edd);
  world.addEntity(tedd);
  world.addEntity(home);
};
