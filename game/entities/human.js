'use strict';
let {setPos, move, moveTo, walk} = require('./humanMethods');
let {pos} = require('./entityProps');

function update(current, delta, msPerUpdate) {
  this.walk(delta);
}

function clientUpdate(current, delta, msPerUpdate) {
  if (this.dest) {
    this.moveTo(delta, this.dest);
  }
}

var human = {
  init(ops) {
    pos(this, ops.pos);
    this.dest = null;
    this.speed = 2;
    this.name = ops.name;
    this.gender = ops.gender;
    return this;
  },
  update, clientUpdate, setPos, move, moveTo, walk
};

module.exports = human;
