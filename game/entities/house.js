'use strict';
let {pos} = require('./entityProps');

function update(current, delta, msPerUpdate) {

}

function clientUpdate(current, delta, msPerUpdate) {

}

let house = {
  init(ops) {
    pos(this, ops.pos);
    return this;
  },
  update, clientUpdate
};

module.exports = house;
