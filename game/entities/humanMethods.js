'use strict';
module.exports = {

  walk(delta) {
    if (this.pos[2] < 2 && this.pos[0] <= 0) this.moveTo(delta, [0, 2, 2]);
    if (this.pos[2] >= 2 && this.pos[0] < 2) this.moveTo(delta, [2, 0, 2]);
    if (this.pos[2] > 0 && this.pos[0] >= 2) this.moveTo(delta, [2, 2, 0]);
    if (this.pos[2] <= 0 && this.pos[0] > 0) this.moveTo(delta, [0, 0, 0]);
    return this;
  },

  setPos(x, y, z) {
    if (typeof x == 'number') this.pos[0] = x;
    if (typeof y == 'number') this.pos[1] = y;
    if (typeof z == 'number') this.pos[2] = z;
    return this;
  },

  moveTo(delta, dest) {
    var
      current = this.pos,
      dx = dest[0] - current[0],
      dy = dest[1] - current[1],
      dz = dest[2] - current[2],
      length = Math.sqrt(dx * dx + dy * dy + dz * dz);

    this.dest = dest;

    dx /= length;
    dy /= length;
    dz /= length;

    this.move(delta, dx, dy, dz);
    return this;
  },

  move(delta, x, y, z) {
    if (typeof x == 'number') this.pos[0] += x * this.speed / delta;
    if (typeof y == 'number') this.pos[1] += y * this.speed / delta;
    if (typeof z == 'number') this.pos[2] += z * this.speed / delta;
    return this;
  }

};
