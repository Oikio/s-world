'use strict';
let human = require('./human');

function update() {
  this.showSecret();
}

function showSecret() {
  this.secret = Math.random();
}

let humanWithSecret = Object.create(human);

Object.assign(humanWithSecret, {
  init(ops) {
    human.init.call(this, ops);
    this.secret = null;
    return this;
  },
  update, showSecret
});

module.exports = humanWithSecret;
