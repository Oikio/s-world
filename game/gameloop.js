'use strict';
module.exports = {
  create(msPerUpdate, update) {
    var prev = Date.now();
    var lag = 0;
    var resume = true;
    var delta = Math.round(1000 / msPerUpdate);

    function run() {
      if(!resume) return;
      var current = Date.now();
      var elapsed = current - prev;
      prev = current;
      lag += elapsed;

      while (lag >= msPerUpdate) {
        update(current, delta, msPerUpdate);
        lag -= msPerUpdate;
      }
      setTimeout(run, msPerUpdate - lag);
    }

    run();

    return {
      set resume(bool) {
        resume = bool;
      },
      get resume () {
        return resume;
      }
    };
  }
};
