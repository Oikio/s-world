'use strict';
const child_process = require('child_process');

module.exports.start = function () {
  var ops = {
    execArgv: ['--harmony_destructuring', '--harmony_default_parameters']
  };
  if (process.env.NODE_ENV !== 'production') ops.execArgv.push('--debug=5859');
  var engine = child_process.fork('./game/gameEngine.js', [], ops);

  process.on('exit', function() {
    console.log('killing engine process\n');
    engine.kill();
  });

  engine.on('message', (message) => {
    sails.sockets.broadcast('game', 'update', message);
  });
};
