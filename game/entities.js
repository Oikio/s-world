module.exports = {
  house: require('./entities/house'),
  human: require('./entities/human'),
  humanWithSecret: require('./entities/humanWithSecret')
};
