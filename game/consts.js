'use strict';

var MS_PER_UPDATE;

if (typeof window === 'undefined') {
  MS_PER_UPDATE = 100;
} else {
  MS_PER_UPDATE = 16.6;
}

module.exports = {MS_PER_UPDATE};