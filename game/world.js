'use strict';
module.exports = {
  entities: require('./entities'),
  objects: [],
  addEntity(obj) {
    this.objects.push(obj);
  },
  removeEntity(obj) {
    this.objects.slice(this.indexOf(obj), 1);
  }
};