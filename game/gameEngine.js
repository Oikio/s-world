'use strict';
const consts = require('./consts');
const gameloop = require('./gameloop');
const world = require('./world');
var mainloop;

function sendWorld(timestamp) {
  process.send({
    timestamp,
    objects: world.objects
});
}

function serverUpdate(current, delta, msPerUpdate) {
  world.objects.forEach(obj => obj.update(current, delta, msPerUpdate));
  sendWorld(current);
}

if (typeof window === 'undefined') {


  require('./initWorld')();

  process.on('message', (message) => console.log('ENGINE received:', message));
  process.on('error', (err) => console.error('ENGINE error:', err));

  mainloop = gameloop.create(consts.MS_PER_UPDATE, serverUpdate);

} else {

}

module.exports = {mainloop};
