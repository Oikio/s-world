'use strict';
import io from 'io';
import B from 'babylonjs/babylon.max';
import consts from 'game/consts';
import {human, house} from 'game/entities';

// openshift websockets support
if (window.location.hostname !== 'localhost') {
  switch (window.location.protocol) {
    case 'http:':
      io.socket.url = `${window.location.protocol}//${window.location.hostname}:8000`;
      break;
    case 'https:':
      io.socket.url = `${window.location.protocol}//${window.location.hostname}:8443`;
      break;
  }
}

io.socket.get('/game/connect', data => console.log(data.message));
io.socket.on('update', data => {

});

document.body.insertAdjacentHTML('afterbegin', '<canvas id="game"></canvas>');

let
  canvas = document.getElementById('game'),
  engine = new B.Engine(canvas, true);

function createScene() {
  let scene = new B.Scene(engine);
  let camera = new B.FreeCamera('camera1', new B.Vector3(0, 5, -10), scene);
  camera.setTarget(B.Vector3.Zero());
  camera.attachControl(canvas, false);
  let light = new B.HemisphericLight('light1', new B.Vector3(0, 1, 0), scene);
  light.intensity = 0.5;
  let ground = B.Mesh.CreateGround('ground1', 6, 6, 2, scene);
  return scene;
}


let
  scene = createScene(),
  msPerUpdate = consts.MS_PER_UPDATE,
  delta = Math.round(1000 / msPerUpdate),
  prev = Date.now(),
  lag = 0;

function update(current, delta, msPerUpdate) {

}

engine.runRenderLoop(function () {
  let current = Date.now();
  let elapsed = current - prev;
  prev = current;
  lag += elapsed;

  while (lag >= msPerUpdate) {
    update(current, delta, msPerUpdate);
    lag -= msPerUpdate;
  }

  scene.render();
});

// Watch for browser/canvas resize events
window.addEventListener('resize', function () {
  engine.resize();
});


