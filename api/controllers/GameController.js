'use strict';

/**
 * GameController
 *
 * @description :: Server-side logic for managing games
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  connect: function (req, res) {
    if (!req.isSocket) {
      return res.badRequest();
    }

    sails.sockets.join(req, 'game', function(err) {
      if (err) {
        return res.serverError(err);
      }

      return res.json({
        message: 'Connected to game!'
      });
    });
  }

};

